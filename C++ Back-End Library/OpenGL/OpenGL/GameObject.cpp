#include "GameObject.h"

GameObject::GameObject() {}

GameObject::~GameObject() {}

void GameObject::setPosition(float x, float y, float z) {
	transform = glm::translate(glm::mat4(1.0f), glm::vec3(x, y, z));
}

void GameObject::update(Timer* timer, Camera* camera, int frontFace) {
	if (physics_)
		transform = physics_->update(timer, &transform);

	if (collision_)
		collision_->update(this, timer);

	if (renderer_)
		renderer_->update(timer, camera, &transform, nullptr, frontFace);
}

void GameObject::add(RendererComponent* renderer) {
	renderer_ = renderer;
}

void GameObject::add(CollisionComponent* collision) {
	collision_ = collision;
}

void GameObject::add(PhysicsComponent* physics) {
	physics_ = physics;
}

bool GameObject::hasPhysics() {
	if (physics_)
		return true;
	else
		return false;
}

bool GameObject::hasCollision() {
	if (collision_)
		return true;
	else
		return false;
}

bool GameObject::hasRenderer() {
	if (renderer_)
		return true;
	else
		return false;
}