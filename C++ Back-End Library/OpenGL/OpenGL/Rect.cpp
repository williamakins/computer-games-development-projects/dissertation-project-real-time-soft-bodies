#include "Rect.h"

Rect::Rect(GLuint* newShader, float width, float height, int mode) {
	std::cout << "Rect object created." << std::endl;

	//save the new render mode param
	renderMode = mode;

	//Per-vertex position vectors
	float rectVerticies[] = {
		-width, height, 0.0f, 1.0f,
		width, height, 0.0f, 1.0f,
		width, -height, 0.0f, 1.0f,
		-width, -height, 0.0f, 1.0f,
	};

	//Per-vertex colours (RGBA) floating point values
	float rectColours[] = {
		1.0f, 0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		1.0f, 0.0f, 1.0f, 1.0f,
	};

	//Each face has 3 vertices(each face forms a triangle)
	unsigned short rectIndicies[] = {
		0, 1, 2, //first triangle (top left - bottom left - top right)
		0, 2, 3 //second triangle (bottom left - top right - bottom right)
	};

	verticies = rectVerticies;
	verticiesLength = sizeof(rectVerticies) / sizeof(*rectVerticies);

	colours = rectColours;
	coloursLength = sizeof(rectColours) / sizeof(*rectColours);

	indicies = rectIndicies;
	indiciesLength = sizeof(rectIndicies) / sizeof(*rectIndicies);

	//pass the shader pointer to the renderer component
	shader = newShader;

	setupShape();
}

Rect::~Rect() {}