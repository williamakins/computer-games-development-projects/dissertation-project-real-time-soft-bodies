#ifndef Plane_H
#define Plane_H

#include "RendererComponent.h"

class Plane : public RendererComponent {
	public:
		Plane(GLuint* newShader, float segWidth, float segHeight, unsigned short rows, unsigned short columns);
		~Plane();
};

#endif