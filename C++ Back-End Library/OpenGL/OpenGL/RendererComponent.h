#ifndef RendererComponent_H
#define RendererComponent_H

#include "Includes.h"

class RendererComponent {
	private:
		GLuint shapeVAO;

		GLuint shapeVertexBuffer;
		GLuint shapeColourBuffer;
		GLuint shapeIndexBuffer;

		// Camera uniforms
		GLint modelMatrixLocation;
		GLint invTransposeMatrixLocation;
		GLint viewProjectionMatrixLocation;
		GLint cameraPosLocation;
	protected:
		int renderMode = GL_TRIANGLES; //set the default mode to render the objects as triangles

		GLuint* shader = nullptr; //the shader that the particular object will use

		float *verticies = nullptr;
		unsigned short verticiesLength = 0;

		float *colours = nullptr;
		unsigned short coloursLength = 0;

		const unsigned short *normals = nullptr;
		unsigned short normalsLength = 0;

		const unsigned short *texCoords = nullptr;
		unsigned short texCoordsLength = 0;

		const unsigned short *indicies = nullptr;
		unsigned short indiciesLength = 0;
	public:
		RendererComponent();
		virtual ~RendererComponent() {};

		void setupShape();

		void updateRenderMode(int);

		void update(Timer*, Camera*, glm::mat4*, GLuint* newTexture = nullptr, int frontFace = GL_CCW);
};

#endif