#ifndef PHYSICSCOMPONENT_H
#define PHYSICSCOMPONENT_H

#include "Includes.h"
#include "Globals.h"
#include "Spring.h"

class PhysicsComponent {
	private:
		//the transform of the game object as a pointer to GameObject
		glm::mat4* transform;

		float _mass = 1.0f;
	public:
		PhysicsComponent();
		~PhysicsComponent();

		void setInitialValues(glm::mat4*, float mass = 1.0f);

		glm::vec3 applyForce();

		glm::mat4 update(Timer*, glm::mat4*);
};

#endif