#include "Cube.h"

Cube::Cube(GLuint* newShader, float width, float height, float depth, int mode) {
	std::cout << "Cube object created." << std::endl;

	//save the new render mode param
	renderMode = mode;

	//Per-vertex position vectors
	float rectVerticies[] = {
		-width, height, -depth, 1.0f,
		-width, height, depth, 1.0f,
		width, height, depth, 1.0f,
		width, height, -depth, 1.0f,
		-width, -height, -depth, 1.0f,
		-width, -height, depth, 1.0f,
		width, -height, depth, 1.0f,
		width, -height, -depth, 1.0f
	};

	//Per-vertex colours (RGBA) floating point values
	float rectColours[] = {
		1.0f, 0.0f, 0.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		1.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 1.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 1.0f, 1.0f,
		1.0f, 0.0f, 1.0f, 1.0f,
		1.0f, 0.0f, 0.0f, 1.0f
	};

	//Each face has 3 vertices(each face forms a triangle)
	unsigned short rectIndicies[] = {
		0, 1, 2, // top (+y)
		0, 2, 3,
		4, 7, 5, // bottom (-y)
		5, 7, 6,
		0, 4, 1, // -x
		1, 4, 5,
		2, 7, 3, // +x
		2, 6, 7,
		0, 3, 4, // -z
		3, 7, 4,
		1, 5, 2, // +z
		2, 5, 6
	};

	verticies = rectVerticies;
	verticiesLength = sizeof(rectVerticies) / sizeof(*rectVerticies);

	colours = rectColours;
	coloursLength = sizeof(rectColours) / sizeof(*rectColours);

	indicies = rectIndicies;
	indiciesLength = sizeof(rectIndicies) / sizeof(*rectIndicies);

	//pass the shader pointer to the renderer component
	shader = newShader;

	setupShape();
}

Cube::~Cube() {}