#include "RendererComponent.h"

RendererComponent::RendererComponent() {
	std::cout << "Initialise the new shape" << std::endl;
}

void RendererComponent::setupShape() {
	// Setup VAO for shape object
	glGenVertexArrays(1, &shapeVAO);
	glBindVertexArray(shapeVAO);

	// Setup VBO for vertex position data
	glGenBuffers(1, &shapeVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, shapeVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, verticiesLength * sizeof(*verticies), verticies, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, nullptr); // attribute 0 gets data from bound VBO (so assign vertex position buffer to attribute 0)

	// Setup VBO for vertex colour data
	glGenBuffers(1, &shapeColourBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, shapeColourBuffer);
	glBufferData(GL_ARRAY_BUFFER, coloursLength * sizeof(*colours), colours, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, 0, nullptr); // attribute 1 gets colour data

	// Enable vertex position and colour attribute arrays
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	// Setup VBO for face index array
	glGenBuffers(1, &shapeIndexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shapeIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indiciesLength * sizeof(*indicies), indicies, GL_STATIC_DRAW);

	// Unbind shape VAO (or bind another VAO for another object / effect)
	// If we didn't do this, we may alter the bindings created above.
	glBindVertexArray(0);

	//for (int i = 0; i < verticiesLength; ++i) {
	//	std::cout << verticies[i] << std::endl;
	//}


	//std::cout << "newwww line " << std::endl;
}

void RendererComponent::update(Timer* timer, Camera* camera, glm::mat4* transform, GLuint* newTexture, int frontFace) {
	if (shapeVAO) {
		//static float newTimer += 1.0f * timer->getDeltaTimeSeconds();

		//if (newTimer >= 1.0f) {
		//	for (int i = 0; i < shapeVertexBuffer; ++i) {
		//		std::cout << verticies[i] << std::endl;
		//		verticies[i] += 0.2f;
		//	}
		//	glBindBuffer(GL_ARRAY_BUFFER, shapeVertexBuffer);
		//	glBufferData(GL_ARRAY_BUFFER, verticiesLength * sizeof(*verticies), verticies, GL_STATIC_DRAW);

		//	newTimer = 0.0f;
		//}

		glm::mat4 view = camera->getViewMatrix();
		glm::mat4 projection = camera->getProjectionMatrix();

		//static float num = 0.0f;
		//num += 0.001f * timer->getDeltaTimeSeconds();
		//verticies[0] = num;
		//verticies[1] = num;
		//for (int i = 0; i < 4; i++) {
		//	verticies[i] = num;
		//}

		glUseProgram(*shader);

		glUniformMatrix4fv(glGetUniformLocation(*shader, "view"), 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(glGetUniformLocation(*shader, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
		glUniformMatrix4fv(glGetUniformLocation(*shader, "model"), 1, GL_FALSE, glm::value_ptr(*transform));

		// Bind VAO that contains all relevant cube VBO buffer and attribute pointer bindings
		glBindVertexArray(shapeVAO);

		// Draw object
		glFrontFace(frontFace);
		glDrawElements(renderMode, indiciesLength, GL_UNSIGNED_SHORT, nullptr);
		glUseProgram(0);

		for (int i = 0; i < verticiesLength; ++i) {
			std::cout << verticies[i] << std::endl;
		}


		std::cout << "newwww line " << std::endl;
	}
}

void RendererComponent::updateRenderMode(int newRenderMode) {
	renderMode = newRenderMode;
}