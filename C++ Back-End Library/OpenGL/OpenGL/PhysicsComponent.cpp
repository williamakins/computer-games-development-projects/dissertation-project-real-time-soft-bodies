#include "PhysicsComponent.h"

PhysicsComponent::PhysicsComponent() {
	//gravity should be passed in
	Spring spring();
}

PhysicsComponent::~PhysicsComponent() {}

void PhysicsComponent::setInitialValues(glm::mat4* modelTransform, float mass) {
	//transform = modelTransform;
	_mass = mass;
}

glm::vec3 PhysicsComponent::applyForce() {
	glm::vec3 newForce = glm::vec3(0.0f, Globals::getGravity(), 0.0f);

	return newForce;
}

glm::mat4 PhysicsComponent::update(Timer* timer, glm::mat4* curTransform) {
	glm::vec3 newForce = applyForce() *= timer->getDeltaTimeSeconds();

	//glm::vec3 newForce = glm::vec3(0.0f, applyForce().y * timer->getDeltaTimeSeconds(), 0.0f);

	return glm::translate(*curTransform, newForce);
}