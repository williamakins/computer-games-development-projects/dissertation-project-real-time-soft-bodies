#ifndef Cube_H
#define Cube_H

#include "RendererComponent.h"

class Cube : public RendererComponent {
	public:
		Cube(GLuint* newShader, float width, float height, float depth, int mode = GL_TRIANGLES);
		~Cube();
};

#endif