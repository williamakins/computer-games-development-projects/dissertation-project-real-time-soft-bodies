#include "Globals.h"

void Globals::setGravity(float newGravity) {
	gravity = newGravity; //gravity in m/s/s
}

float Globals::getGravity() {
	return gravity;
}

//vector<GLuint*>* Globals::getCollsionObjects() {
//	return &collisionObjects;
//}

//void Globals::addNewCollisionObject(GLuint* newObj) {
//	collisionObjects.push_back(newObj);
//}

vector<GameObject*>* Globals::getSceneObjects() {
	return &sceneObjects;
}

void Globals::AddNewSceneObject(GameObject* newObj) {
	sceneObjects.push_back(newObj);
	NumSceneObjects++;
}

void Globals::incrementNumSceneObjects(int num) {
	NumSceneObjects += num;
}

int Globals::getNumSceneObjects() {
	return NumSceneObjects;
}

float Globals::gravity;
int Globals::NumSceneObjects;
vector<GameObject*> Globals::sceneObjects;
//vector<GLuint*> Globals::collisionObjects;