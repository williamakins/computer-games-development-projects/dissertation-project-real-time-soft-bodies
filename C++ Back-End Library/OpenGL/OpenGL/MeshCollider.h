#ifndef MeshCollider_H
#define MeshCollider_H

#include "CollisionComponent.h"

class MeshCollider : public CollisionComponent {
	public:
		MeshCollider();
		~MeshCollider();
};

#endif