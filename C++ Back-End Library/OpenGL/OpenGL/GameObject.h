#ifndef GameObject_H
#define GameObject_H

#include "RendererComponent.h"
#include "CollisionComponent.h"
#include "PhysicsComponent.h"
#include "Timer.h"

class GameObject {
	private:
		//the transform of the game object
		glm::mat4 transform = glm::mat4(1.0f);

	public:
		PhysicsComponent* physics_ = nullptr;
		CollisionComponent* collision_ = nullptr;
		RendererComponent* renderer_ = nullptr;
		
		//GameObject(RendererComponent* renderer = nullptr) : renderer_(renderer) {};
		GameObject();
		~GameObject();

		void setPosition(float x = 0.0f, float y = 0.0f, float z = 0.0f);

		void update(Timer*, Camera*, int frontFace = GL_CCW);

		void add(PhysicsComponent* physics = nullptr);
		void add(CollisionComponent* collision = nullptr);
		void add(RendererComponent* renderer = nullptr);

		bool hasPhysics();
		bool hasCollision();
		bool hasRenderer();
};

#endif