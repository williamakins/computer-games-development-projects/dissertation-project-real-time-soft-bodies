#ifndef CollisionComponent_H
#define CollisionComponent_H

#include "Includes.h"
#include "Globals.h"

class CollisionComponent {
	private:
	public:
		CollisionComponent();
		~CollisionComponent();

		void checkIntersection(GameObject*, GameObject*);

		void update(GameObject*, Timer*);
};

#endif