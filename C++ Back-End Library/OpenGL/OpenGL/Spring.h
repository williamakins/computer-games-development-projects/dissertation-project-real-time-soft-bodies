#ifndef Spring_H
#define Spring_H

#include "Includes.h"

class Spring {
	private:
		float _elasticity = 0.0f;
		float _springConstant = 1.0f;

		float restLength = 1.0f;
		float extendedLength = 2.0f;

		glm::vec3 springPos;
		const glm::mat4* modelTransform;
	public:
		Spring(glm::vec3 startPos, const glm::mat4* modelTransform);
		~Spring();

		void update();
};

#endif