#include "Plane.h"

#define PI 3.14159265

Plane::Plane(GLuint* newShader, float segWidth, float segHeight, unsigned short rows, unsigned short columns) {
	std::cout << "plane object created with " << rows << " row segments and "  << columns << " column segments." << std::endl;

	int i = 0;
	int rowLen = rows + 1;
	int columnLen = columns + 1;

	//specify total number of verts (x,y,z,w in a single array)
	unsigned short totalVertices = (rowLen * columnLen) * 4;
	//specify the total number of indicies
	int totalIndicies = ((rows * 2) + 2) * columns + ((columns - 1) * 2);

	//generate the arrays to store the vertex data
	float* planeVertices = new float[totalVertices];
	unsigned short *planeIndicies = new unsigned short[totalIndicies];
	float* planeColours = new float[totalVertices];

	//save the new render mode param
	renderMode = GL_TRIANGLE_STRIP;

	float yOffset = 0;
	float num = 0;
	float time = 0.0f;
	//fill the vert array
	for (int x = 0; x < rowLen; x++) {
		for (int y = 0; y < columnLen; y++) {
			yOffset = sin(5 * (x / 0.1)) * cos(5 * (y / 0.1)) / 5;

			planeVertices[i++] = (float)segWidth * x; //x
			planeVertices[i++] = yOffset; //y
			planeVertices[i++] = (float)segHeight * y; //z
			planeVertices[i++] = 1.0f; //w
		}
	}

	i = 0;

	//fill the indicies array
	for (int row = 0; row < rowLen; row++) {
		for (int col = 0; col < columnLen; col++) {
			planeIndicies[i++] = col + row * rowLen;
			planeIndicies[i++] = col + (row + 1) * rowLen;

			//create degenerate points in order to stitch together multiple triangle strips in a single render pass
			if ((col == columns) && row < rows - 1) {
				planeIndicies[i++] = col + (row + 1) * rowLen;
				planeIndicies[i++] = (col + row * rowLen) + 1;
			}
		}
	}

	i = 0;

	//Per-vertex colours (RGBA) floating point values
	//additionally the size of the colour array will be equal to the verts so no need to store the colour length
	for (int x = 0; x < totalVertices / 4; x++) {
		//planeColours[i++] = (float)rand() / (RAND_MAX); //R
		//planeColours[i++] = (float)rand() / (RAND_MAX); //G
		//planeColours[i++] = (float)rand() / (RAND_MAX); //B

		planeColours[i++] = 0.0f; //R
		planeColours[i++] = planeVertices[i] + 0.2f; //G
		planeColours[i++] = 0.0f; //B
		planeColours[i++] = 1.0f; //A
	}

	//for (int i = 0; i < totalIndicies; i++) {
	//	std::cout << i << ": " << planeIndicies[i] << std::endl;
	//}

	std::cout << "Vertices: " << totalVertices << std::endl;
	std::cout << "Indicies: " << totalIndicies << std::endl;
	std::cout << "Colours: " << totalVertices << std::endl;

	verticies = planeVertices;
	verticiesLength = totalVertices;

	indicies = planeIndicies;
	indiciesLength = totalIndicies;

	colours = planeColours;
	coloursLength = totalVertices;

	//pass the shader pointer to the renderer component
	shader = newShader;

	setupShape();
}

Plane::~Plane() {
	//delete[] verticies;
	//delete[] indicies;
	//delete[] colours;
}
