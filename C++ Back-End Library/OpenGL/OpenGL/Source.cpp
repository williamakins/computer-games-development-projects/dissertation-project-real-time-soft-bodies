#include "Includes.h"

#include "Globals.h"
#include "GameObject.h"

#include "Plane.h"
#include "Cube.h"
#include "Sphere.h"
#include "Rect.h"

#include "MeshCollider.h"
#include "BoxCollider.h"

#include "RigidBody.h"
#include "SoftBody.h"

// Function prototypes
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);

// Camera settings
//width, heigh, near plane, far plane
Camera_settings camera_settings{ 1280, 720, 0.1, 100.0 };

//Timer
Timer timer;

// Instantiate the camera object with basic data
Camera camera(camera_settings);

float lastX = camera_settings.screenWidth / 2.0f;
float lastY = camera_settings.screenHeight / 2.0f;

int main() {
	// glfw: initialize and configure
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// glfw window creation
	GLFWwindow* window = glfwCreateWindow(camera_settings.screenWidth, camera_settings.screenHeight, "OpenGL rendering of soft-body simulations | William Akins 15029476", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	// Set the callback functions
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);

	// tell GLFW to capture our mouse
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	// glad: load all OpenGL function pointers
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	//Rendering settings
	glfwSwapInterval(0); //glfw enable swap interval to match screen v-sync
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE); //Enables face culling
	glFrontFace(GL_CCW);//Specifies which winding order if front facing

	//build and compile our shader program
	GLuint basic_shader;
	GLSL_ERROR glsl_err = ShaderLoader::createShaderProgram(
		string("Resources\\Shaders\\Basic_shader.vert"),
		string("Resources\\Shaders\\Basic_shader.frag"),
		&basic_shader);

	//initialise the gravity for the scene
	Globals::setGravity(-1.0f);

	//GameObject *myTerrain = new GameObject();
	//Globals::AddNewSceneObject(myTerrain);
	//myTerrain->add(new Plane(&basic_shader, 0.1, 0.1, 50, 50));
	//myTerrain->setPosition(0.0f, 0.0f, 0.0f);
	//myTerrain->add(new RigidBody());
	//myTerrain->add(new BoxCollider());

	GameObject *myRigidCube = new GameObject();
	Globals::AddNewSceneObject(myRigidCube);
	myRigidCube->add(new Cube(&basic_shader, 4.0f, 0.1f, 4.0f));
	myRigidCube->setPosition(0.0f, 0.0f, 0.0f);
	//myRigidCube->add(new RigidBody());
	myRigidCube->add(new MeshCollider());

	GameObject *myRect = new GameObject();
	Globals::AddNewSceneObject(myRect);
	myRect->add(new Rect(&basic_shader, 4.0f, 1.0f, GL_TRIANGLES));
	myRect->setPosition(0.0f, 1.0f, -4.0f);

	GameObject *myCube = new GameObject();
	Globals::AddNewSceneObject(myCube);
	myCube->add(new Cube(&basic_shader, 0.2f, 0.2f, 0.2f));
	myCube->setPosition(0.0f, 2.0f, 0.0f);
	myCube->add(new SoftBody());
	myCube->add(new MeshCollider());

	//for (int i = 0; i < Globals::getSceneObjects()->size(); i++) {
	//	std::cout << Globals::getSceneObjects()->at(i)->hasCollision() << std::endl;
	//}

	//std::cout << "VAO = " << myCube->returnVAO() << std::endl;

	// render loop
	while (!glfwWindowShouldClose(window)) {
		// input
		processInput(window);
		timer.tick();

		// render
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//draw the shape on the screen
		//myTerrain->update(&timer, &camera, GL_CW);
		myRigidCube->update(&timer, &camera);
		myRect->update(&timer, &camera, GL_CW);

		myCube->update(&timer, &camera);

		string title = "FPS: " + std::to_string(timer.averageFPS()) + " SPF: " + std::to_string(timer.currentSPF());
		glfwSetWindowTitle(window, title.c_str());

		// glfw: swap buffers and poll events
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// glfw: terminate, clearing all previously allocated GLFW resources.
	glfwTerminate();
	return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
void processInput(GLFWwindow *window) {
	timer.updateDeltaTime();

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.processKeyboard(FORWARD, timer.getDeltaTimeSeconds());
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.processKeyboard(BACKWARD, timer.getDeltaTimeSeconds());
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.processKeyboard(LEFT, timer.getDeltaTimeSeconds());
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.processKeyboard(RIGHT, timer.getDeltaTimeSeconds());

	//allow the rendering mode to be switched
	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
		for (int i = 0; i < Globals::getSceneObjects()->size(); i++) {
			Globals::getSceneObjects()->at(i)->renderer_->updateRenderMode(GL_TRIANGLES);
		}
	}

	if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
		for (int i = 0; i < Globals::getSceneObjects()->size(); i++) {
			Globals::getSceneObjects()->at(i)->renderer_->updateRenderMode(GL_LINE_LOOP);
		}
	}

}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
	// make sure the viewport matches the new window dimensions; note that width and 
	glViewport(0, 0, width, height);
	camera.updateScreenSize(width, height);
}

// glfw: whenever the mouse moves, this callback is called
void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
	float xoffset = xpos - lastX;
	float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

	lastX = xpos;
	lastY = ypos;

	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
		camera.processMouseMovement(xoffset, yoffset);
	}
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
	camera.processMouseScroll(yoffset);
}