#ifndef Rect_H
#define Rect_H

#include "RendererComponent.h"

class Rect : public RendererComponent {
	public:
		Rect(GLuint* newShader, float width, float height, int mode = GL_TRIANGLES);
		~Rect();
};

#endif