#ifndef GLOBALS_H
#define GLOBALS_H

#include "Includes.h"

class GameObject;

class Globals {
	private:
		static float gravity; //gravity in m/s/s

		//stores a pointer to each of the collision onjects within the scene
		static vector<GameObject*> sceneObjects;

		//stores a pointer to each of the collision onjects within the scene
		//static vector<GLuint*> collisionObjects;

		//number of objects currently in the scene
		static int NumSceneObjects;

	public:
		static void setGravity(float newGravity = 9.80665);
		static float getGravity();

		//static vector<GLuint*>* getCollsionObjects();
		//static void addNewCollisionObject(GLuint*);

		static vector<GameObject*>* getSceneObjects();
		static void AddNewSceneObject(GameObject*);

		static void incrementNumSceneObjects(int);
		static int getNumSceneObjects();
};

#endif