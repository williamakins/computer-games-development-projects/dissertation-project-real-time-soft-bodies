#ifndef RIGIDBODY_H
#define RIGIDBODY_H

#include "PhysicsComponent.h"

class RigidBody : public PhysicsComponent {
	public:
		RigidBody();
		~RigidBody();
};

#endif