﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float mouseSpeed = 2.0f;
    public float movementSpeed = 0.1f;

    private float mouseYaw = 0.0f;
    private float mousePitch = 0.0f;

    private bool allowMouseControl = false;

    private RaycastHit hit;



    // Use this for initialization
    private void Start ()
    {
		
	}

    // Update is called once per frame
    private void Update ()
    {
		if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            allowMouseControl = !allowMouseControl;


            if (allowMouseControl)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }

        if (allowMouseControl)
        {
            mouseYaw += mouseSpeed * Input.GetAxis("Mouse X");
            mousePitch -= mouseSpeed * Input.GetAxis("Mouse Y");

            transform.eulerAngles = new Vector3(mousePitch, mouseYaw, 0.0f);
        }

        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(new Vector3(0.0f, 0.0f, movementSpeed));
        }
        else if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(new Vector3(0.0f, 0.0f, -movementSpeed));
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(new Vector3(-movementSpeed, 0.0f, 0.0f));
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(new Vector3(movementSpeed, 0.0f, 0.0f));
        }


        if (!allowMouseControl)
        {
            //hit = new RaycastHit();
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "PhysicsObject" && Input.GetMouseButton(0))
                {
                    Debug.Log("Physics Object clicked on");

                    Vector3 screenPoint = Camera.main.WorldToScreenPoint(hit.transform.position);
                    Vector3 offset = hit.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

                    hit.transform.GetComponent<DragObject>().setDragValues(screenPoint, offset);
                }
            }
        }
    }
}