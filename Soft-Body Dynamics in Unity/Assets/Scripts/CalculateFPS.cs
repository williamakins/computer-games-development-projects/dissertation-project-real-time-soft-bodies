﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalculateFPS : MonoBehaviour
{
    private float frameCount = 0.0f;
    private float dt = 0.0f;
    private float fps = 0.0f;
    private float updateRate = 0.5f;
    private float avgDt = 0.0f;

    // Use this for initialization
    private void Start ()
    {
        //nextUpdate = Time.time;
    }

    private void Update()
    {
        frameCount++;
        dt += Time.deltaTime;
        if (dt > 1.0f / updateRate)
        {
            Debug.Log("avg dt: " + (dt / frameCount));

            fps = frameCount / dt;
            frameCount = 0.0f;

            Debug.Log("avg fps: " + fps);

            dt -= 1.0f / updateRate;
        }
    }
}