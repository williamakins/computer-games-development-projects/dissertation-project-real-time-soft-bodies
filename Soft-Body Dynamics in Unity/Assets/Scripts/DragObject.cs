﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObject : MonoBehaviour {

    private Vector3 screenPoint;
    private Vector3 offset;

    public void setDragValues(Vector3 newScreenPoint, Vector3 newOffset)
    {
        screenPoint = newScreenPoint;
        offset = newOffset;
    }

    void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;

        transform.position = curPosition;
    }
}