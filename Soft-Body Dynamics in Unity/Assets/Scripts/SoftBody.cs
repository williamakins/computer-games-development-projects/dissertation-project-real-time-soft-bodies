﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoftBody : MonoBehaviour
{
    public float mass = 2.0f;
    public float damping = 0.4f;
    public float gravity = -9.81f;

    public float springConstant = 3.01f;

    //private float restLength = 1.0f;

    private float duration = 0.0f;

    private MeshFilter objMesh;
    MeshCollider collider;

    private Vector3[] meshVerticies;
    private Vector3 force = new Vector3(0.0f, 0.0f, 0.0f);

    private Vector3[] velocity;

    private float[] restLength;

    // Use this for initialization
    void Start ()
    {
        objMesh = transform.GetComponent<MeshFilter>();
        meshVerticies = objMesh.mesh.vertices;

        velocity = new Vector3[meshVerticies.Length];
        restLength = new float[meshVerticies.Length];

        for (int i = 0; i < meshVerticies.Length; i++)
        {
            velocity[i] = Vector3.zero;

            //constrain the current and next vertex positions within the mesh bounds
            int curPoint;
            int nextPoint;
            if (i != meshVerticies.Length - 1)
            {
                curPoint = i;
                nextPoint = i + 1;
            }
            else
            {
                curPoint = i;
                nextPoint = 0;
            }

            restLength[i] = Vector3.Distance(meshVerticies[curPoint], meshVerticies[nextPoint]);
        }

        collider = transform.GetComponent<MeshCollider>();
        collider.sharedMesh = objMesh.mesh;
    }

    // Update is called once per frame
    private void Update()
    {
        //float duration = 5.0f;

        duration += Time.deltaTime;

        updateSpring();


        ////calculate magnitude
        //float magnitude = force.magnitude;
        //magnitude = -springConstant * (magnitude - restLength);

        ////apply the force
        //force.Normalize();
        //force *= (force / force.le) * magnitude - (40000 * velocity);
        ////particleRB.AddForce(force);

        //addForce(force);

        ////Debug.Log(force);

        //ensure spring representation follows the particle
        //points[1] = particle.transform.position;

        //update the line with the current positions
        //lr.SetPositions(points);
    }

    private void updateSpring()
    {
        for (int i = 0; i < meshVerticies.Length; i++)
        {
            //constrain the current and next vertex positions within the mesh bounds
            int curPoint;
            int nextPoint;
            if (i != meshVerticies.Length - 1)
            {
                curPoint = i;
                nextPoint = i + 1;
            }
            else
            {
                curPoint = i;
                nextPoint = 0;
            }

            Vector3 force = meshVerticies[nextPoint];
            force -= meshVerticies[curPoint]; //anchor point

            float magnitude = force.magnitude;

            float displacement = magnitude - restLength[i];
            magnitude = -1 * springConstant * displacement;

            force.Normalize();
            force *= magnitude;

            //apply gravity
            float mg = (mass * gravity);
            //force += new Vector3(0.0f, mg, 0.0f);

            meshVerticies[curPoint] += force * Time.deltaTime;
        }
        objMesh.mesh.vertices = meshVerticies;
        collider.sharedMesh = objMesh.mesh;
    }

    private void addForce(Vector3 force, int i)
    {
        //float fg = mass * gravity;

        //apply the gravitional force
        //force = new Vector3(force.x, force.y + fg, force.z);

        //Debug.Log(force);

        meshVerticies[i] += (force * Time.deltaTime);
        objMesh.mesh.vertices = meshVerticies;
    }
}