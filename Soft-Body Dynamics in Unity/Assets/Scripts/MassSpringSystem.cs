﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassSpringSystem : MonoBehaviour
{
    public Color sphereColourHigh;
    public Color sphereColourMed;
    public Color sphereColourLow;
    public Color sphereColourNone;

    public int width = 3;
    public int height = 3;

    public Vector3 particleScale = new Vector3(0.5f, 0.5f, 0.5f);
    //public float springLength = 2.0f;

    public float mass = 1.0f;
    public float damping = 0.7f;
    public float gravity = -9.81f;
    public float springConstant = 2.7f;
    public float maxLength = 5.0f;
    public float restLength = 1.0f;

    private Vector3[][][] points;
    private GameObject[][] particle;
    private Vector3[][] force;
    private LineRenderer[][] lr;

    private Rigidbody[] rb;

    private int numOfParticles = 1;

    // Use this for initialization
    void Start()
    {
        numOfParticles = width * height;

        int numOfSprings = ((width * height) - width) * 2;

        lr = new LineRenderer[(width + width) - 1][];
        for (int i = 0; i < lr.Length; i++)
            lr[i] = new LineRenderer[(height + height) - 1];

        points = new Vector3[(width + width) - 1][][];
        for (int i = 0; i < points.Length; i++)
            points[i] = new Vector3[(height + height) - 1][];

        //populate the particle 2D array
        particle = new GameObject[width][];
        for (int i = 0; i < particle.Length; i++)
            particle[i] = new GameObject[height];

        force = new Vector3[width][];
        for (int i = 0; i < force.Length; i++)
            force[i] = new Vector3[height];

        rb = new Rigidbody[numOfParticles];

        Vector3 particlePos = transform.position;
        Vector3 springPos = new Vector3(0.0f, 0.0f, 0.0f);

        int num = 0;
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                particle[x][y] = Instantiate(Resources.Load("Sphere"), particlePos, Quaternion.identity) as GameObject;
                particle[x][y].transform.SetParent(transform);
                particle[x][y].transform.localScale = particleScale;

                rb[num] = particle[x][y].GetComponent<Rigidbody>();

                particlePos += new Vector3(restLength, 0.0f, 0.0f);

                num++;
            }
            particlePos = new Vector3(0.0f, particlePos.y + restLength, particlePos.z);
        }

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int springX = x + x;
                int springY = y + y;

                //Down
                if (x < width - 1 && springX < points.Length)
                {
                    springX++;
                    createSpringRender(springX, springY, x, y, false, true);
                }

                //Across
                if (y < height - 1 && springY < points[1].Length)
                {
                    springY++;
                    createSpringRender(springX, springY, x, y, true, true);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        updateSpring();

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int springX = x + x;
                int springY = y + y;

                //Down
                if (x < width - 1 && springX < points.Length)
                {
                    springX++;
                    createSpringRender(springX, springY, x, y, false, false);
                }

                //Across
                if (y < height - 1 && springY < points[1].Length)
                {
                    springY++;
                    createSpringRender(springX, springY, x, y, true, false);
                }
            }
        }
    }

    private void updateSpring()
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                //position of this particle within the spring array
                int springX = x + x;
                int springY = y + y;

                Vector3 totalForce = Vector3.zero;

                int numOfSprings = 0;
                for (int i = 0; i < 4; i++)
                {
                    Vector3[] springPos = getSpringPos(springX, springY, i);

                    if (springPos != null)
                    {
                        numOfSprings++;
                    }
                }

                for (int i = 0; i < 4; i++)
                {
                    Vector3[] springPos = getSpringPos(springX, springY, i);
                    
                    if (springPos != null)
                    {
                        force[x][y] = springPos[1];
                        force[x][y] -= springPos[0]; //anchor point

                        float magnitude = force[x][y].magnitude;

                        float displacement = magnitude - restLength;

                        float k = springConstant * numOfSprings;

                        magnitude = -1 * k * displacement;

                        force[x][y].Normalize();
                        force[x][y] *= magnitude;



                        if (Vector3.Distance(springPos[0], springPos[1]) > maxLength)
                        {
                            force[x][y] = Vector3.zero;
                        }

                        totalForce += force[x][y];
                    }
                }

                //apply gravity
                float mg = (mass * gravity);
                totalForce += new Vector3(0.0f, mg, 0.0f);

                particle[x][y].transform.GetComponent<MeshRenderer>().material.color = updateParticleColour(Mathf.Abs(totalForce.magnitude));

                particle[x][y].transform.Translate(totalForce * Time.deltaTime);
            }
        }
    }

    private void createSpringRender(int springX, int springY, int particleX, int particleY, bool springDirAcross, bool needsRendering)
    {
        if (needsRendering)
        {
            GameObject spring = Instantiate(Resources.Load("Spring"), transform.position, Quaternion.identity) as GameObject;
            spring.transform.SetParent(transform);
            lr[springX][springY] = spring.GetComponent<LineRenderer>();

            points[springX][springY] = new Vector3[lr[springX][springY].positionCount];
            lr[springX][springY].GetPositions(points[springX][springY]);
        }

        points[springX][springY][0] = particle[particleX][particleY].transform.position;
        if (springDirAcross == false)
            points[springX][springY][1] = particle[particleX + 1][particleY].transform.position;
        else
            points[springX][springY][1] = particle[particleX][particleY + 1].transform.position;

        lr[springX][springY].SetPositions(points[springX][springY]);
    }

    private Color updateParticleColour(float magnitude)
    {
        if (magnitude <= 0.0f)
            return sphereColourNone;
        else if (magnitude > 0.0f && magnitude < 1.0f)
            return sphereColourLow;
        else if (magnitude > 1.0f && magnitude < 2.0f)
            return sphereColourMed;
        else if (magnitude > 2.0f)
            return sphereColourHigh;
        else
            return sphereColourNone;
    }

    private Vector3[] getSpringPos(int x, int y, int i)
    {
        Vector3[] point = null;
        switch (i)
        {
            case 0:
                if (y + 1 < points[1].Length)
                    point  = points[x][y + 1];
                return point; //y + i
            case 1:
                if (y - 1 >= 0)
                    point = points[x][y - 1];
                return point; //y - i
            case 2:
                if (x + 1 < points[1].Length)
                    point = points[x + 1][y];
                return point; //x + i
            case 3:
                if (x - 1 >= 0)
                    point = points[x - 1][y];
                return point; //x - i
            default:
                return point;
        }
    }
}