﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassSpring : MonoBehaviour
{
    public float mass = 2.0f;
    public float damping = 0.4f;
    public float gravity = -9.81f;
    public float springConstant = 0.01f;
    public float restLength = 1.0f;
    public float maxLength = 5.0f;

    private float duration = 0.0f;

    //private float dampingRatio = 0.9f; //Damping ratio (Zeta)
    //private float freq = 0.1f; //Angular frequency (Omega)

    //the cxurrent velocity of the spring
    //private Vector3 pos = new Vector3(0.0f, 0.0f, 0.0f);
    private Vector3 velocity = new Vector3(0.0f, 0.0f, 0.0f);
    //private float acceleration = 0.0f;

    private Vector3[] points;

    private GameObject particle;
    //private Rigidbody particleRB;

    private LineRenderer lr;

    // Use this for initialization
    private void Start ()
    {
        particle = transform.Find("BasicSphere").gameObject;
        //particleRB = particle.GetComponent<Rigidbody>();

        lr = transform.GetComponent<LineRenderer>();
        points = new Vector3[lr.positionCount];
        lr.GetPositions(points);

        points[1] = points[0];
        points[1] += new Vector3(0.0f, restLength, 0.0f);

        restLength = Vector3.Distance(points[0], points[1]);
        //Debug.Log(restLength);
    }

    // Update is called once per frame
    private void Update ()
    {
        //float duration = 5.0f;

        duration += Time.deltaTime;

        updateSpring();


        ////calculate magnitude
        //float magnitude = force.magnitude;
        //magnitude = -springConstant * (magnitude - restLength);

        ////apply the force
        //force.Normalize();
        //force *= (force / force.le) * magnitude - (40000 * velocity);
        ////particleRB.AddForce(force);

        //addForce(force);

        ////Debug.Log(force);

        //ensure spring representation follows the particle
        points[1] = particle.transform.position;

        //particle.transform.position = points[1];

        //update the line with the current positions
        lr.SetPositions(points);
    }

    private void updateSpring()
    {
        Vector3 force = points[1];
        force -= points[0]; //anchor point

        float magnitude = force.magnitude;

        float displacement = magnitude - restLength;
        magnitude = -1 * springConstant * displacement;

        force.Normalize();
        force *= magnitude;

        //apply gravity
        float mg = (mass * gravity);
        force += new Vector3(0.0f, mg, 0.0f);

        //Debug.Log("force= " + displacement);

        particle.transform.Translate(force * Time.deltaTime);

        //velocity = position * gamma * Mathf.Cos(gamma * duration);


        //Vector3 target = position * Mathf.Cos(gamma * duration) + c * Mathf.Sin(gamma * duration);

        //target *= Mathf.Exp(-0.5f * duration * damping);

        //Vector3 acceleration = (target - position) * (1.0f / (duration * duration)) - velocity * duration;

        //particle.transform.Translate((acceleration * mass) * Time.deltaTime);

        //particle.transform.position += acceleration * Time.deltaTime;

        //particle.transform.position += (acceleration * mass) * Time.deltaTime;

        //Debug.Log(velocity);

        //float deltaTime = Time.deltaTime;

        //Vector3 displacement = pos - points[0];

        //float gamma = freq * Mathf.Sqrt(1.0f - (dampingRatio * dampingRatio));
        //float cos = Mathf.Cos(gamma * deltaTime);
        //float sin = Mathf.Sin(gamma * deltaTime);
        //float exp = Mathf.Exp(-deltaTime * (freq * dampingRatio));

        //Vector3 c = velocity + (displacement * (freq * dampingRatio)) / gammma;

        //pos = particle.transform.position + exp * (cos * displacement + sin * c);

        //velocity = -exp * ((displacement * (freq * dampingRatio) - c * gamma) * cos + (displacement * gamma + c * (freq * dampingRatio)) * sin);

        //particle.transform.position = pos;

        //Debug.Log(velocity);

        //particle.transform.position += (force * Time.deltaTime);

        //float magnitude = force.magnitude;
        //float displacement = magnitude - restLength;

        //force.Normalize();
        //force *= -springConstant * displacement;

        //Debug.Log(force);

        //addForce(force);


        //

        //float currentLength = magnitude - restLength;

        //magnitude = -springConstant * currentLength;

        //force.Normalize();
        //force *= magnitude;




        //force /= mass;

        //float currentLength = Mathf.Abs(restLength - magnitude);
        //float currentLength =  magnitude - restLength;
        //float currentLength = Mathf.Abs(restLength - magnitude);
        //Debug.Log(currentLength);

        //float velocity = -(0.5f * mass) * ((magnitude - restLength) * (magnitude - restLength));

        //magnitude = Mathf.Abs(magnitude - restLength);
        //magnitude *= springConstant;


        //Vector3 springForce = new Vector3(0.0f, 0.0f, 0.0f);
        //Vector3 springForce = -springConstant * (particle.transform.position - restLength);

        //Vector3 dampedForce = velocity * damping;

        //force.Normalize();
        //force *= -magnitude;

        //force = new Vector3(force.x, force.y + (mass * gravity), force.z);


        //force -= dampedForce;


        //Vector3 acceleration = force / mass;

        //velocity += acceleration * Time.deltaTime;



        //Debug.Log(particle.transform.position);



        //force *= -magnitude;

        //addForce(force);
    }

    private void constainSpring()
    {
        //float currentLength = points[1] - points[0];
    }
}