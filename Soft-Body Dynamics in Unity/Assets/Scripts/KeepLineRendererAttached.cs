﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepLineRendererAttached : MonoBehaviour
{
    private GameObject particle;
    private LineRenderer lr;
    private Vector3[] points;

    // Use this for initialization
    void Start ()
    {
        particle = transform.Find("BasicSphere").gameObject;

        lr = transform.GetComponent<LineRenderer>();
        points = new Vector3[lr.positionCount];
        lr.GetPositions(points);
    }
	
	// Update is called once per frame
	void Update ()
    {
        //ensure spring representation follows the particle
        points[1] = particle.transform.position;

        //update the line with the current positions
        lr.SetPositions(points);
    }
}