﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassSpringMultiple : MonoBehaviour
{
    public int numOfParticles = 1;

    public float mass = 2.0f;
    public float damping = 0.4f;
    public float gravity = -9.81f;
    public float springConstant = 0.01f;
    public float maxLength = 5.0f;
    public float restLength = 1.0f;

    private Vector3[][] points;
    private GameObject[] particle;

    private LineRenderer[] lr;

    private Vector3[] force;

    // Use this for initialization
    void Start()
    {
        lr = new LineRenderer[numOfParticles];
        particle = new GameObject[numOfParticles];
        points = new Vector3[numOfParticles][];
        force = new Vector3[numOfParticles];

        for (int i = 0; i < numOfParticles; i++)
        {
            particle[i] = transform.Find("Spring" + i + "/BasicSphere").gameObject;

            lr[i] = transform.Find("Spring" + i).GetComponent<LineRenderer>();
            points[i] = new Vector3[lr[i].positionCount];

            lr[i].GetPositions(points[i]);

            //restLength[i] = Mathf.Abs(points[i][0].magnitude - points[i][1].magnitude);

            points[i][1] = points[i][0];
            points[i][1] += new Vector3(0.0f, restLength, 0.0f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        updateSpring();

        //ensure spring representation follows the particles
        for (int i = 0; i < numOfParticles; i++)
        {
            if (i > 0)
            {
                points[i][0] = particle[i - 1].transform.position;
            }

            points[i][1] = particle[i].transform.position;

            //update the line with the current positions
            lr[i].SetPositions(points[i]);
        }
    }

    private void updateSpring()
    {
        for (int i = 0; i < numOfParticles; i++)
        {
            force[i] = points[i][1];
            force[i] -= points[i][0]; //anchor point

            float magnitude = force[i].magnitude;

            float displacement = magnitude - restLength;

            if (displacement > maxLength)
            {
                displacement = maxLength;
            }

            magnitude = -1 * springConstant * displacement;

            force[i].Normalize();
            force[i] *= magnitude;

            if (i < numOfParticles - 1)
            {
                force[i] -= force[i + 1];
            }

            //apply gravity
            float mg = (mass * gravity);
            force[i] += new Vector3(0.0f, mg, 0.0f);

            //Debug.Log("force= " + displacement);

            particle[i].transform.Translate(force[i] * Time.deltaTime);
        }
    }

    private void moveOtherParticles()
    {

    }
}