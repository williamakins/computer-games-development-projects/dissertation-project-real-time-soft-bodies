﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassSpringRigidBody : MonoBehaviour
{
    public float mass = 2.0f;
    public float damping = 0.4f;
    public float gravity = -9.81f;

    public float springConstant = 0.001f;

    private float restLength = 1.0f;

    private Vector3[] points;

    private GameObject particle;
    private Rigidbody particleRB;

    private LineRenderer lr;

    // Use this for initialization
    private void Start()
    {
        particle = transform.Find("BasicSphere").gameObject;
        particleRB = particle.GetComponent<Rigidbody>();

        lr = transform.GetComponent<LineRenderer>();
        points = new Vector3[lr.positionCount];
        lr.GetPositions(points);

        restLength = Vector3.Distance(points[0], points[1]);
        //Debug.Log(restLength);
    }

    // Update is called once per frame
    private void Update()
    {
        Vector3 force = particle.transform.position;
        force -= points[0]; //anchor point

        //calculate magnitude
        float magnitude = force.magnitude;
        magnitude = springConstant * Mathf.Abs((magnitude - restLength));

        //apply the force
        force.Normalize();
        force *= -magnitude;
        particleRB.AddForce(force);

        //ensure spring representation follows the particle
        points[1] = particle.transform.position;

        //update the line with the current positions
        lr.SetPositions(points);
    }
}