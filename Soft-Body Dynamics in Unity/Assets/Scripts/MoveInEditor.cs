﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class MoveInEditor : MonoBehaviour
{
    private LineRenderer lr;
    private GameObject particle;
    private Vector3[] points;

    // Use this for initialization
    void Start ()
    {
        particle = transform.Find("BasicSphere").gameObject;

        lr = transform.GetComponent<LineRenderer>();
        points = new Vector3[lr.positionCount];
        lr.GetPositions(points);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!EditorApplication.isPlaying)
        {
            //ensure spring representation follows the particle
            points[1] = particle.transform.position;
            points[0] = new Vector3(points[1].x, points[0].y, points[1].z);

            //update the line with the current positions
            lr.SetPositions(points);
        }
    }
}

#endif